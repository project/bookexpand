
1.  Upload the directory of all of the files to the server into the modules folder of your website.
2.  Navigate to admin/modules and enable the 'bookexpand' module.
3.  Navigate to admin/settings/bookexpand and set the filter to whichever option you prefer.

Each new page that is created will now be forced through one of those filters as explained in README.txt.  

Warning:
If you already have pre-existing groups, you will need to do additional work to get this module to work properly.  See README.txt for some instructions.