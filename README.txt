This module allows the administrator of a site (or anyone with access to the settings) to add a 
filter to the creation of new pages.  The choices of filter are:

1.	none
2.	by role
3.	by user
4.	by Organic Group

For the 4th filter, this module requires an additional table in the database, which it calls 'og_book'.  
Unless you have used another module that has created this table, this should not create a conflict. However,
if your users have already created a bunch of pages in their Organic Groups, this module is not able to 
automatically recognize those pages.  If you already have OG installed, and users have created a bunch of
 pages, this module can still work for you.  What you will need to do is to create parent book pages for 
each existing group (new groups will automatically have these pages created).  Fortunately this version of
 the module comes with a script that -should- do this for you.  Make a backup of your database, then navigate
to the settings page for this module and click go!.

When any of these filters are enabled (the default setting is none), any user (except user 1) is only 
able to add pages to either:

1.	none - add pages anywhere to any book which is default behaviour
2.	by role - add pages to any book created by a user with the same role as the current user
3.	by user - add pages to any book created by the current user, should enable new book creation 
                  for users or this will not allow any new pages to be added
4.	by Organic Group - uses the create new book page link provided by OG to find the current group id,
                           new book pages created elsewhere will default to none as the filter.

A new option with this module is to define a default filter of user.  This is especially useful if you want
users to only be able to create books in the context of their own personal book.

